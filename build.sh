#!/bin/bash

#                  _               _           _ _     _           
#    __ _ _ __ ___(_) __ _  ___   | |__  _   _(_) | __| | ___ _ __ 
#   / _' | '__/ __| |/ _' |/ _ \  | '_ \| | | | | |/ _' |/ _ \ '__|
#  | (_| | | | (__| | (_| | (_) | | |_) | |_| | | | (_| |  __/ |   
#   \__,_|_|  \___|_|\__, |\___/  |_.__/ \__,_|_|_|\__,_|\___|_|
#                    |___/                                         

# A automated shell script to build custom arcigo linux environment.

# Version: 0.4 -- by arcigo (https://gitlab.com/arcigo-linux/iso-builder/)
# Revision: 2022.06.08
# (GNU/General Public License version 3.0)

LCLST="en_US"
# Format is language_COUNTRY where language is lower case two letter code
# and country is upper case two letter code, separated with an underscore

KEYMP="us"
# Use lower case two letter country code

KEYMOD="pc105"
# pc105 and pc104 are modern standards, all others need to be researched

MYUSERNM="live"
# use all lowercase letters only

MYUSRPASSWD="arcigo"
# Pick a password of your choice

RTPASSWD="toor"
# Pick a root password

MYHOSTNM="arcigo"
# Pick a hostname for the machine

SCRIPT_VER="0.4"
# Script version

function _bold_text(){
  echo -e "\n\033[1m${1}\033[0m\n"
}

function _red_text(){
  echo -e "\n\033[0;31m${1}\033[0m\n"
}

function _bold_red_text(){
  echo -e "\n\033[1;31m${1}\033[0m\n"
}

function _green_text(){
  echo -e "\n\033[0;32m${1}\033[0m\n"
}

function _bold_green_text(){
  echo -e "\n\033[1;32m${1}\033[0m\n"
}

# Check if the script run as root
check_root () {
  if [[ "$EUID" != 0 ]]; then
    _bold_red_text "[-] Please run as root!"
    sleep 2 && exit
  fi
}

# Welcome banner
banner () {
  clear
  _bold_text "
                   _               _           _ _     _           
     __ _ _ __ ___(_) __ _  ___   | |__  _   _(_) | __| | ___ _ __ 
    / _' | '__/ __| |/ _' |/ _ \  | '_ \| | | | | |/ _' |/ _ \ '__|
   | (_| | | | (__| | (_| | (_) | | |_) | |_| | | | (_| |  __/ |   
    \__,_|_|  \___|_|\__, |\___/  |_.__/ \__,_|_|_|\__,_|\___|_| v${SCRIPT_VER}
                     |___/                                         "
}

# Display line error
_error_handling () {
  set -uo pipefail
  trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR
}

# Clean working directories if exists
cleanup () {
  _bold_text "==> Checking if there is any working directories to be cleaned"
  sleep 2
  [[ -d ./releng ]] && rm -r ./releng
  [[ -d ./work ]] && rm -r ./work
  [[ -d ./out ]] && mv ./out ./out-bak && _green_text "==> 'out' folder has moved to 'out-bak'..."
  sleep 2
}

# Check dependencies for ISO build 
_check_dependencies () {
  local online_ver local_ver packages

  packages=("archlinux-keyring" "arcigo-keyring" "archiso" "mkinitcpio-archiso")

  _bold_text "  ==================================================================="
  _bold_text "==> Checking dependencies and preparing... "
  for i in ${packages[@]}; do
    local_ver=$(pacman -Q ${i} | cut -d' ' -f2)
    online_ver=$(pacman -S --print-format %v ${i})

    if [[ $local_ver != $online_ver ]]; then
      echo -e "\n==> Installing ${i}... \n"
      pacman -S --needed --noconfirm ${i}
    fi
  done
  sleep 2
  _bold_green_text "==> Dependencies are installed and up-to-date. !"
}

# Copying releng to working directory
_copy_releng () {
  _bold_text "==> Copying releng to working directory..."
  sleep 2
  cp -r /usr/share/archiso/configs/releng/ ./releng
  rm -r ./releng/efiboot
  rm -r ./releng/syslinux
  rm -r ./releng/airootfs/etc/ssh
}

# Delete automatic login
_delete_auto_login () {
  _bold_text "==> Deleting automatic login..." && sleep 2
  [[ -d ./releng/airootfs/etc/systemd/system/getty@tty1.service.d ]] && rm -r ./releng/airootfs/etc/systemd/system/getty@tty1.service.d
}

# Remove cloud-init and other services
_rm_unwanted_services () {
  _green_text "\n==> Removing cloud-init and other services... \n"
  sleep 2
  rm ./releng/airootfs/etc/systemd/system/multi-user.target.wants/hv_fcopy_daemon.service
  rm ./releng/airootfs/etc/systemd/system/multi-user.target.wants/hv_kvp_daemon.service
  rm ./releng/airootfs/etc/systemd/system/multi-user.target.wants/hv_vss_daemon.service
  rm ./releng/airootfs/etc/systemd/system/multi-user.target.wants/qemu-guest-agent.service
  rm ./releng/airootfs/etc/systemd/system/multi-user.target.wants/vmtoolsd.service
  rm ./releng/airootfs/etc/systemd/system/multi-user.target.wants/vmware-vmblock-fuse.service
  rm ./releng/airootfs/etc/systemd/system/multi-user.target.wants/sshd.service
  rm ./releng/airootfs/etc/systemd/system/multi-user.target.wants/iwd.service
  rm -r ./releng/airootfs/etc/systemd/system/cloud-init.target.wants
}

# Add Bluetooth, cups, haveged, NetworkManager, & sddm systemd service links
_add_services () {
  _bold_text "==> Adding services... "
  mkdir -p ./releng/airootfs/etc/systemd/system/network-online.target.wants
  mkdir -p ./releng/airootfs/etc/systemd/system/multi-user.target.wants
  mkdir -p ./releng/airootfs/etc/systemd/system/bluetooth.target.wants
  mkdir -p ./releng/airootfs/etc/systemd/system/printer.target.wants
  mkdir -p ./releng/airootfs/etc/systemd/system/sockets.target.wants
  mkdir -p ./releng/airootfs/etc/systemd/system/timers.target.wants
  mkdir -p ./releng/airootfs/etc/systemd/system/sysinit.target.wants
  ln -sf /usr/lib/systemd/system/NetworkManager-wait-online.service ./releng/airootfs/etc/systemd/system/network-online.target.wants/NetworkManager-wait-online.service
  ln -sf /usr/lib/systemd/system/NetworkManager-dispatcher.service ./releng/airootfs/etc/systemd/system/dbus-org.freedesktop.nm-dispatcher.service
  ln -sf /usr/lib/systemd/system/NetworkManager.service ./releng/airootfs/etc/systemd/system/multi-user.target.wants/NetworkManager.service
  ln -sf /usr/lib/systemd/system/bluetooth.service ./releng/airootfs/etc/systemd/system/bluetooth.target.wants/bluetooth.service
  ln -sf /usr/lib/systemd/system/haveged.service ./releng/airootfs/etc/systemd/system/sysinit.target.wants/haveged.service
  ln -sf /usr/lib/systemd/system/cups.service ./releng/airootfs/etc/systemd/system/printer.target.wants/cups.service
  ln -sf /usr/lib/systemd/system/cups.socket ./releng/airootfs/etc/systemd/system/sockets.target.wants/cups.socket
  ln -sf /usr/lib/systemd/system/cups.path ./releng/airootfs/etc/systemd/system/multi-user.target.wants/cups.path
  ln -sf /usr/lib/systemd/system/bluetooth.service ./releng/airootfs/etc/systemd/system/dbus-org.bluez.service
  ln -sf /usr/lib/systemd/system/sddm.service ./releng/airootfs/etc/systemd/system/display-manager.service
}

# Copy files to customize the ISO
_cp_to_iso () {
  _bold_green_text "==> Customizing the ISO... " && sleep 2
  cp packages.x86_64 ./releng/
  cp pacman.conf ./releng/
  cp profiledef.sh ./releng/
  cp -r grub ./releng/
  cp -r root ./releng/airootfs/
  cp -r efiboot ./releng/
  cp -r syslinux ./releng/
  cp -r usr ./releng/airootfs/
  cp -r etc ./releng/airootfs/
  cp -r opt ./releng/airootfs/
  rm ./releng/grub/grub.cfg
  cp grub/grub.cfg ./releng/grub/grub.cfg
  ln -sf /usr/share/arcigo-docs ./releng/airootfs/etc/skel/arcigo-docs
}

# Set hostname
_set_hostname () {
  _bold_text "==> Creating hostname file..."
  sleep 1
  echo "${MYHOSTNM}" > ./releng/airootfs/etc/hostname
}

# Create passwd file
_set_passwd () {
  _bold_text "==> Creating password file..." && sleep 1
echo "root:x:0:0:root:/root:/usr/bin/bash
"${MYUSERNM}":x:1010:1010:Liveuser:/home/"${MYUSERNM}":/bin/bash" > ./releng/airootfs/etc/passwd
}

# Create group file
_set_group () {
  _bold_text "==> Creating group file..." && sleep 1
echo "root:x:0:root
sys:x:3:"${MYUSERNM}"
adm:x:4:"${MYUSERNM}"
wheel:x:10:"${MYUSERNM}"
log:x:19:"${MYUSERNM}"
network:x:90:"${MYUSERNM}"
floppy:x:94:"${MYUSERNM}"
scanner:x:96:"${MYUSERNM}"
power:x:98:"${MYUSERNM}"
rfkill:x:850:"${MYUSERNM}"
users:x:985:"${MYUSERNM}"
video:x:860:"${MYUSERNM}"
storage:x:870:"${MYUSERNM}"
optical:x:880:"${MYUSERNM}"
lp:x:840:"${MYUSERNM}"
audio:x:890:"${MYUSERNM}"
"${MYUSERNM}":x:1010:" > ./releng/airootfs/etc/group
}

# Create shadow file
_set_shadow () {
  local usr_hash root_hash
  _bold_text "==> Creating shadow file..." && sleep 1
  usr_hash=$(openssl passwd -6 "${MYUSRPASSWD}")
  root_hash=$(openssl passwd -6 "${RTPASSWD}")
echo "root:"${root_hash}":14871::::::
"${MYUSERNM}":"${usr_hash}":14871::::::" > ./releng/airootfs/etc/shadow
}

# create gshadow file
_set_gshadow () {
  _bold_text "==> Creating gshadow file..." && sleep 1
echo "root:!*::root
"${MYUSERNM}":!*::" > ./releng/airootfs/etc/gshadow
}

# Set the keyboard layout
_set_keylayout () {
  _bold_text "==> Setting default keyboard layout as ${KEYMP}..." && sleep 1
  echo "KEYMAP="${KEYMP}"" > ./releng/airootfs/etc/vconsole.conf
}

# Download bashrc config
check_bashrc () {
  if [[ -f etc/skel/.bashrc ]]; then
    echo -e "\n==> Bashrc exist ! checking again for newer version"
    sleep 3
    rm ./etc/skel/.bashrc
    wget -nc https://gitlab.com/arcigo-linux/arcigo-bashrc-config/-/raw/main/bashrc
    mv bashrc etc/skel/.bashrc
    echo -e "\n==> bashrc updated !"
  fi
}

# Create 00-keyboard.conf file
_crt_keyboard () {
  _bold_text "==> Creating X11 config for keyboard..." && sleep 2
  if [[ ! -d ./releng/airootfs/etc/X11/xorg.conf.d ]]; then
    mkdir -p ./releng/airootfs/etc/X11/xorg.conf.d
  fi
echo "Section \"InputClass\"
        Identifier \"system-keyboard\"
        MatchIsKeyboard \"on\"
        Option \"XkbLayout\" \""${KEYMP}"\"
        Option \"XkbModel\" \""${KEYMOD}"\"
EndSection" > ./releng/airootfs/etc/X11/xorg.conf.d/00-keyboard.conf
  _bold_text "==> Creating X11 config for touchpad..." && sleep 2
echo "Section "InputClass"
        Identifier      "Synaptics touchpads"
        MatchIsTouchpad "on"
        Driver          "synaptics"
        Option "TapButton1" "1"
        Option "TapButton2" "3"
        Option "TapButton3" "2"
        Option "VertEdgeScroll" "on"
        Option "VertTwoFingerScroll" "on"
        Option "HorizEdgeScroll" "on"
        Option "HorizTwoFingerScroll" "on"
        Option "CircularScrolling" "on"
        Option "CircScrollTrigger" "2"
        Option "EmulateTwoFingerMinZ" "40"
        Option "EmulateTwoFingerMinW" "8"
        Option "CoastingSpeed" "0"
        Option "FingerLow" "30"
        Option "FingerHigh" "50"
        Option "MaxTapTime" "125"
EndSection" > ./releng/airootfs/etc/X11/xorg.conf.d/10-synaptics.conf
}

# Fix 40-locale-gen.hook and create locale.conf
_crt_locales () {
  _bold_text "==> Generating locales and fixing the locale hooks" && sleep 2
  echo "LANG="${LCLST}".UTF-8" > ./releng/airootfs/etc/locale.conf
  sed -i "s/en_US/"${LCLST}"/g" ./releng/airootfs/etc/pacman.d/hooks/40-locale-gen.hook
}

# Start mkarchiso to build ISO
build_iso () {
  _bold_green_text "==> Building the Arcigo Linux ISO..."
  sleep 3
  mkarchiso -v -w ./work -o ./out ./releng
}

# ----------------------------------------
# Run Functions
# ----------------------------------------
banner
check_root
_error_handling
_check_dependencies
cleanup
_copy_releng
_add_services
# check_bashrc
_delete_auto_login
_rm_unwanted_services
_cp_to_iso
_set_hostname
_set_passwd
_set_group
_set_shadow
_set_gshadow
_set_keylayout
_crt_keyboard
_crt_locales
build_iso


# Disclaimer:
#
# THIS SOFTWARE IS PROVIDED BY ARCIGO “AS IS” AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
# EVENT SHALL ARCIGO BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
# END
#
